package com.dto;

import java.util.List;

public class MenuDto {
    private Integer id;
    private String text;
    private String iconCls;
    private Boolean checked;
    private String url;
    private String description;
    private String role;
    private Integer _parentId;
    private boolean st;
    private boolean action;
    private List<MenuDto> children;
    private String state;
    
    
    
    
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public List<MenuDto> getChildren() {
        return children;
    }
    public void setChildren(List<MenuDto> children) {
        this.children = children;
    }
    public boolean isAction() {
        return action;
    }
    public void setAction(boolean action) {
        this.action = action;
    }
    
    public boolean isSt() {
        return st;
    }
    public void setSt(boolean st) {
        this.st = st;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    public String getIconCls() {
        return iconCls;
    }
    public void setIconCls(String iconCls) {
        this.iconCls = iconCls;
    }
    public Boolean getChecked() {
        return checked;
    }
    public void setChecked(Boolean checked) {
        this.checked = checked;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    public Integer get_parentId() {
        return _parentId;
    }
    public void set_parentId(Integer _parentId) {
        this._parentId = _parentId;
    }
    
}
