package com.dao.impl;

import org.springframework.stereotype.Repository;

import com.dao.ArticleDao;
import com.entity.Article;
@Repository("articleDao")
public class ArticleDaoImpl extends BaseDaoImpl<Article> implements ArticleDao{

}
