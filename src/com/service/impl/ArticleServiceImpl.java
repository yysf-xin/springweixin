package com.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.Restrictions;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.dao.ArticleDao;
import com.dao.ArticleTypeDao;
import com.dao.HtmlDao;
import com.dao.NewsDao;
import com.dto.ArticleDto;
import com.dto.Pageinfo;
import com.dto.Pagers;
import com.entity.Article;
import com.entity.Articletype;
import com.entity.Html;
import com.entity.News;
import com.entity.User;
import com.service.ArticleService;
import com.vo.ArticleVo;

/**
 * 文章服务实现类 ArticleServiceImpl.java
 * 
 * @author microxdd
 * @time 创建时间：2014 2014年9月13日 下午1:07:12
 * @version
 * @user micrxdd
 */
@Service("articleService")
public class ArticleServiceImpl implements ArticleService {
    @Resource
    private ArticleDao articleDao;
    @Resource
    private ArticleTypeDao articleTypeDao;
    @Resource
    private NewsDao newsDao;
    @Resource
    private HtmlDao htmlDao;

    private Whitelist whitelist;
    private Whitelist simpleText;

    public ArticleServiceImpl() {
	// TODO Auto-generated constructor stub
	// html 过滤
	whitelist = Whitelist.relaxed();
	whitelist.addTags("embed", "object", "param", "span", "div", "video");
	simpleText = new Whitelist();
    }

    @Override
    public Pagers ArticleListById(Pageinfo pageinfo, Integer id) {
	// TODO Auto-generated method stub
	Articletype articletype = new Articletype();
	articletype.setId(id);
	Pagers pagers = articleDao.getForPage(pageinfo,
		Restrictions.eq("articletype", articletype));
	List<Article> articles = (List<Article>) pagers.getRows();
	List<ArticleDto> dtos = new ArrayList<ArticleDto>();
	for (Article article : articles) {
	    ArticleDto dto = new ArticleDto();
	    BeanUtils.copyProperties(article, dto, "html");
	    dto.setUsername(article.getUser().getUsername());
	    dtos.add(dto);
	}
	pagers.setRows(dtos);
	return pagers;
    }

    @Override
    public void SaveArticle(ArticleVo articleVo, User user) {
	// TODO Auto-generated method stub
	if(articleVo.getId()!=null&&articleVo.getId()>0){
	    UpdateArticle(articleVo,user);
	    return;
	}
	System.out.println("保存");
	Articletype  articletype=articleTypeDao.findById(articleVo.getTypeid());
	if(articletype!=null){
	    
	    //分开保存 避免 因为外键的原因 hibernate出现多条update语句
	    
	    Article article=new Article();
	    article.setArticletype(articletype);
	    article.setIsshow(Boolean.TRUE);
	    article.setCtime(new Timestamp(System.currentTimeMillis()));
	    System.out.println(user+user.getUsername()+user.getPassword()+"id:"+user.getId());
	    article.setUser(user);
	    article.setTitle(articleVo.getTitle());
	    
	    articleDao.save(article);
	    
	    Html html=new Html();
	    //html.setArticle(article);
	    
	    String us=articleVo.getHtml();
	    
	    html.setHtml(us);
	    
	    html.setArticle(article);
	    
	    htmlDao.save(html);
	    
	    String des=Jsoup.clean(us,simpleText).trim();
	    
	    if(des.length()>40){
		des=des.substring(0,50);
	    }
	    
	    //article.setHtml(html);
	    //获取图片链接
	    Document doc=Jsoup.parse(us);
	    Elements elements=doc.select("img");
	    String picUrl="/img/nopic.jpg";
	    System.out.println("图片链接");
	    for (Element element : elements) {
		System.out.println(element.attr("src"));
		picUrl=element.attr("src");
		if(picUrl==null||picUrl.equals("")){
		    continue;
		}
		break;
	    }
	    News news=new News();
	    news.setArticleid(article.getId());
	    news.setArticle(article);
	    news.setDescription(des);
	    news.setTitle(article.getTitle());
	    //news.setUrl("http://localhost:8080/"+article.getId());
	    news.setPicUrl(picUrl);
	    news.setArticleid(article.getId());
	    news.setArticle(article);
	    //article.setNews(news);
	    
	    //设置根分类   方便首页展示的时候索引。
	    news.setArticletypeid(RootArticletype(articletype).getId());
	    newsDao.save(news);
	    //articleDao.save(article);
	    //htmlDao.save(html);
	    
	}
	
    }

    @Override
    public Article FindById(Integer id) {
	// TODO Auto-generated method stub
	return articleDao.findById(id);
    }

    @Override
    public ArticleVo FindVoById(Integer id) {
	// TODO Auto-generated method stub
	Article article = articleDao.findById(id);
	ArticleVo vo = null;
	if (article != null) {
	    vo = new ArticleVo();
	    vo.setId(article.getId());
	    vo.setTitle(article.getTitle());
	    vo.setHtml(article.getHtml().getHtml());
	    vo.setTypeid(article.getArticletype().getId());
	}

	return vo;
    }

    @Override
    public void UpdateArticle(ArticleVo articleVo, User user) {
	// TODO Auto-generated method stub
	Article article = articleDao.findById(articleVo.getId());
	System.out.println("更新");
	Html html = article.getHtml();
	html.setHtml(articleVo.getHtml());
	Articletype articletype = articleTypeDao
		.findById(articleVo.getTypeid());
	article.setArticletype(articletype);
	article.setCtime(new Timestamp(System.currentTimeMillis()));
	article.setTitle(articleVo.getTitle());
    }

    public Articletype RootArticletype(Articletype articletype) {
	Articletype a = articletype.getArticletype();
	if (a != null) {
	    return RootArticletype(a);
	} else {
	    return articletype;
	}
    }

    @Override
    public void DelArticle(Integer[] ids) {
	// TODO Auto-generated method stub
	for (Integer id : ids) {
	    Article article=articleDao.findById(id);
	    articleDao.del(article);
	}
    }
}
