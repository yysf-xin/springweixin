package com.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.dao.NewsItemDao;
import com.dao.NewsTypeDao;
import com.dto.NewsTypeDto;
import com.dto.Pageinfo;
import com.entity.Newsitem;
import com.entity.Newstype;
import com.service.NewsTypeService;
import com.vo.NewsTypeVo;
/**
 * 新闻分类服务
 * NewsTypeServiceImpl.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月13日 下午4:27:55 
 * @version
 * @user micrxdd
 */
@Service("newsTypeService")
public class NewsTypeServiceImpl implements NewsTypeService{
    @Resource
    private NewsTypeDao newsTypeDao;
    @Resource
    private NewsItemDao newsItemDao;
    @Override
    public List<NewsTypeDto> NewsTypeList() {
	// TODO Auto-generated method stub
	List<Newstype> list=newsTypeDao.findAll();
	return f(list);
    }
    
    public List<NewsTypeDto> f(List<Newstype> list){
	List<NewsTypeDto> newsTypeDtos = null;
	if (list.size() > 0) {
	    Map<Integer, NewsTypeDto> map=new HashMap<Integer, NewsTypeDto>();
	    newsTypeDtos = new ArrayList<NewsTypeDto>();
	    for (Newstype newstype : list) {
		NewsTypeDto vo = new NewsTypeDto();
		BeanUtils.copyProperties(newstype, vo,"newstype");
		List<NewsTypeDto> child = new ArrayList<NewsTypeDto>();
		vo.setChildren(child);
		map.put(newstype.getId(), vo);
	    }
	    for (Newstype newstype : list) {
		Newstype pNewstype=newstype.getNewstype();
		if(pNewstype==null){
		    System.out.println("根节点:"+newstype.getText());
		    newsTypeDtos.add(map.get(newstype.getId()));
		}else{
		    System.out.println("设置叶子节点:"+newstype.getText()+">"+pNewstype.getText());
		    map.get(pNewstype.getId()).getChildren().add(map.get(newstype.getId()));
		}
	    }
	}
	return newsTypeDtos;
    }

    @Override
    public void SaveNewsType(NewsTypeVo newsTypeVo) {
	// TODO Auto-generated method stub
	Integer id=newsTypeVo.getId();
	if(id!=null&&id>0){
	    UpdateNewsType(newsTypeVo);
	    return;
	}
	Integer pid=newsTypeVo.getPid();
	Newstype newstype=new Newstype();
	newstype.setText(newsTypeVo.getName());
	newsTypeDao.save(newstype);
	if(pid!=null){
	    Newstype newstype2=newsTypeDao.findById(pid);
	    if(newstype2!=null){
		newstype.setNewstype(newstype2);
		int total= newsTypeDao.getCount(Restrictions.eq("newstype", newstype2));
		System.out.println(total);
		if(total==1){
		    newsItemDao.UpdateNewsType(newstype2, newstype);
		}
		    
	    }
	}
	
    }

    @Override
    public void UpdateNewsType(NewsTypeVo newsTypeVo) {
	// TODO Auto-generated method stub
	Newstype newstype=newsTypeDao.findById(newsTypeVo.getId());
	newstype.setText(newsTypeVo.getName());
	Integer pid=newsTypeVo.getPid();
	if(pid!=null){
	    Newstype newstype2=newsTypeDao.findById(pid);
	    newstype.setNewstype(newstype2);
	}
    }

    @Override
    public void DelNewsType(Integer id) {
	// TODO Auto-generated method stub
	newsTypeDao.DelById(id);
    }

    @Override
    public NewsTypeVo NewsTypeById(Integer id) {
	// TODO Auto-generated method stub
	Newstype newstype=newsTypeDao.findById(id);
	NewsTypeVo newsTypeVo=null;
	if(newstype!=null){
	    newsTypeVo=new NewsTypeVo();
	    newsTypeVo.setId(id);
	    newsTypeVo.setName(newstype.getText());
	    Newstype pnewstype=newstype.getNewstype();
	    if(pnewstype!=null){
		newsTypeVo.setPid(pnewstype.getId());
	    }
	    
	}
	
	return newsTypeVo;
    }

    @Override
    public List<Newstype> NewsTypeRootList(Pageinfo pageinfo) {
	// TODO Auto-generated method stub
	return newsTypeDao.getForPageList(pageinfo, Restrictions.isNull("newstype"));
    }

}
