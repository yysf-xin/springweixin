package com.service;

import java.util.List;

import com.dto.Pageinfo;
import com.dto.Pagers;
import com.dto.TextDto;
/**
 * 文本服务接口
 * TextService.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月14日 下午2:58:55 
 * @version
 * @user micrxdd
 */
public interface TextService {
    /**
     * 返回文本消息列表
     * @param pageinfo
     * @return
     */
    public Pagers textMsgList(Pageinfo pageinfo);
    /**
     * 保存文本消息
     * @param dtos
     */
    public void SaveTextMsg(List<TextDto> dtos);
    /**
     * 更新文本消息
     * @param dtos
     */
    public void UpdateTextMsg(List<TextDto> dtos);
    /**
     * 删除文本消息
     * @param ids
     */
    public void DelTextMsg(Integer[] ids);
}
