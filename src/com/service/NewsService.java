package com.service;
import java.util.List;

import com.dto.NewsDto;
import com.dto.Pageinfo;
import com.dto.Pagers;
import com.entity.News;
/**
 * 新闻消息服务
 * NewsService.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月13日 下午10:03:10 
 * @version
 * @user micrxdd
 */
public interface NewsService {
    /**
     * 根据新闻条目id返回消息列表
     * @param pageinfo
     * @param itemid
     * @return
     */
    public Pagers NewsList(Pageinfo pageinfo,Integer itemid);
    /**
     * 返回没有被使用的消息
     * @param pageinfo
     * @return
     */
    public Pagers NewsListNoItem(Pageinfo pageinfo);
    /**
     * 保存新闻消息
     * @param dto
     * @param itemid
     */
    public void SaveNews(NewsDto dto,Integer itemid);
    /**
     * 更新新闻消息(此处不是更新数据，而是更新关联对象)
     * @param ids
     * @param itemsid
     */
    public void UpdateNews(Integer[] ids,Integer itemsid);
    /**
     * 删除新闻消息
     * @param ids
     */
    public void DelNewsIds(Integer[] ids);
    /**
     * 保存新闻消息(此处是保存链接式的消息)
     * @param dto
     */
    public void SaveNewsDtos(List<NewsDto> dto);
    /**
     * 更新新闻消息
     * @param dto
     */
    public void UpdateNewsDtos(List<NewsDto> dto);
    /**
     * 根据文章的分类id 获取该聚合内容
     * @param pageinfo
     */
    public List<News> NewsCounts(Pageinfo pageinfo,Integer typeid);
    /**
     * 获取最新的新闻
     * @param pageinfo
     * @return
     */
    public List<News> NewsByNew(Pageinfo pageinfo);
}
