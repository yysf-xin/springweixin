package com.weixin.util;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class XmlUtil {
	/**
	 * 将xml转换成javabean
	 * @param xml
	 * @param class1
	 * @return 这个类的实例
	 */
	
	@SuppressWarnings("unchecked")
	public static <T> T XmltoBean(String xml,Class<T> class1){
		T t=null;
		try {
			JAXBContext jc = JAXBContext.newInstance(class1);
			Unmarshaller u = jc.createUnmarshaller();
			t= (T) u.unmarshal(new StringReader(xml));
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return t;
	}

}
